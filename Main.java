import java.util.*;
import java.util.function.*;

/// Run-time polymorphism in the form of generics is not possible for this
/// sadly. Later we will use HOF polymorphism.
class Operations {
  public static long DEFAULT_VALUE = 0L;

  /// Encapsulation: direct access to `value` is not allowed outside; it's
  /// moderated by getters and setters.
  private long value;

  /// Overloaded constructors "compile-time polymorphism"
  Operations () {
    this.value = DEFAULT_VALUE;
  }

  Operations (long value) {
    this.value = value;
  }

  public Long value () {
    return this.value;
  }

  /// Encapsulating changes to `value` this way promotes the builder pattern.
  public Operations setValue (long value) {
    this.value = value;
    return this;
  }

  /// One level of abstraction: the +, - primitive operators are hidden behind
  /// method calls
  public void add (Long other) {
    this.value += other;
  }

  public void subtract (Long other) {
    this.value -= other;
  }

  public void multiply (Long other) {
    this.value *= other;
  }
}

/// Inheritance: Extending the behaviour of a superclass as a subclass
/// and only constructing with super() because we do not need to use a custom
/// constructor.
class AdvancedOperations extends Operations {
  AdvancedOperations () {
    super();
  }

  AdvancedOperations (Long value) {
    super(value);
  }

  public void divide (Long other) {
    this.setValue(this.value() / other);
  }

  public void pow (Long other) {
    this.setValue((long) Math.pow(this.value(), other));
  }
}

class Evaluator {
  private AdvancedOperations accumulator;
  private long register = AdvancedOperations.DEFAULT_VALUE;

  Evaluator () {
    this.accumulator = new AdvancedOperations(AdvancedOperations.DEFAULT_VALUE);
  }

  Evaluator (long value) {
    this.accumulator = new AdvancedOperations(value);
  }

  /// Explicit value polymorphism: Since Java doesn't allow formal
  /// run-time value polymorphism like patten oriented langauges,
  /// it's done manually via higher-order functions.
  /// i.e. I would ideally use a non-numeric enum of operations, etc
  private Consumer<Long> translateOperation (Character source) {
    switch (source.charValue()) {
      case '+': {
        return accumulator::add;
      }
      case '-': {
        return accumulator::subtract;
      }
      case '*': {
        return accumulator::multiply;
      }
      case '/': {
        return accumulator::divide;
      }
      case '^': {
        return accumulator::pow;
      }
      default: {
        throw new IllegalArgumentException(source.toString());
      }
    }
  }

  public long accumulator () {
    return accumulator.value();
  }

  public long register () {
    return this.register;
  }

  /// protected access to setRegister discourages encapsulation-breaking access
  protected void setRegister (long value) {
    this.register = value;
  }

  /// Encapsulation / Abstraction: caller does not need to know any details of
  /// the implementation to use this method
  public Evaluator run (String operations) {
    final var atoms = operations.split(" ");

    for (var atom: atoms) {
      System.out.printf("ACC: %s  \tREG: %s  \tAtom: %s\n", this.accumulator.value(), this.register, atom);
      if (atom.chars().allMatch(Character::isDigit)) {
        this.setRegister(Long.parseLong(atom));
      } else {
        /// Another level of abstraction: the operation dispatch is formalised
        /// and handled by a private implementation detail
        final var operation = this.translateOperation(atom.charAt(0));
        operation.accept(this.register());
      }
    }

    return this;
  }
}

class Main {
  public static void main (String args[]) {
    final String test_operations = "3 + 2 * 3 - 6 ^ 3 /";

    System.out.printf("Testing '%s'\n", test_operations);

    final Evaluator ev = new Evaluator()
                         .run(test_operations);

    /// Encapsulation / Abstraction: caller does not need to know the details of
    /// the `accumulator` property
    System.out.printf("Final result: %s\n", ev.accumulator());
  }
}
